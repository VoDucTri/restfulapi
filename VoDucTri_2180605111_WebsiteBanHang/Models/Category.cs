﻿using System.ComponentModel.DataAnnotations;

namespace VoDucTri_2180605111_WebsiteBanHang.Models
{
    public class Category
    {
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string Name { get; set; }
        public List<Product>? Products { get; set; }
    }
}

