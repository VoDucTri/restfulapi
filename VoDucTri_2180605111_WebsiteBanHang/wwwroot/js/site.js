﻿// Please see documentation at https://learn.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
// Sử dụng Fetch API hoặc Axios94 BÀI 6: RESTful API
// Lấy danh sách sản phẩm
fetch('http://localhost:5273/api/products')
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to fetch products.');
        }
        return response.json();
    })
    .then(products => {
        // Xử lý danh sách sản phẩm
        products.forEach(product => {
            console.log("Name:", product.name);
            console.log("Price:", product.price);
            console.log("Description:", product.description);
            console.log("Image URL:", product.imageUrl);
            console.log("-------------------");
        });
    })
    .catch(error => console.error('Error fetching products:', error));

// Lấy thông tin chi tiết của sản phẩm có id = 1
const productId = 1;
fetch(`http://localhost:5273/api/products/${productId}`)
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to fetch product details.');
        }
        return response.json();
    })
    .then(product => {
        // Xử lý thông tin chi tiết sản phẩm
        console.log("Product Details:");
        console.log("Name:", product.name);
        console.log("Price:", product.price);
        console.log("Description:", product.description);
        console.log("Image URL:", product.imageUrl);
        console.log("-------------------");
    })
    .catch(error => console.error('Error fetching product details:', error));

// Tạo một sản phẩm mới
const newProduct = {
    name: 'New Product',
    price: 100,
    description: 'A new product',
    imageUrl: '/images/new_product.jpg', // Thêm URL hình ảnh tương ứng của sản phẩm mới
};
fetch('http://localhost:5273/api/products', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(newProduct),
})
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to create new product.');
        }
        return response.json();
    })
    .then(createdProduct => {
        // Xử lý thông tin sản phẩm đã tạo
        console.log("New Product Created:");
        console.log("ID:", createdProduct.id);
        console.log("Name:", createdProduct.name);
        console.log("Price:", createdProduct.price);
        console.log("Description:", createdProduct.description);
        console.log("Image URL:", createdProduct.imageUrl);
        console.log("-------------------");
    })
    .catch(error => console.error('Error creating new product:', error));

// Cập nhật thông tin của sản phẩm có id = 1
const productIdToUpdate = 1;
const updatedProduct = {
    name: 'Updated Product',
    price: 150,
    description: 'An updated product',
    imageUrl: '/images/updated_product.jpg', // Thêm URL hình ảnh tương ứng của sản phẩm được cập nhật
};
fetch(`http://localhost:5273/api/products/${productIdToUpdate}`, {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(updatedProduct),
})
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to update product.');
        }
        console.log('Product updated successfully.');
    })
    .catch(error => console.error('Error updating product:', error));

// Xóa sản phẩm có id = 1
const productIdToDelete = 1;
fetch(`http://localhost:5273/api/products/${productIdToDelete}`, {
    method: 'DELETE',
})
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to delete product.');
        }
        console.log('Product deleted successfully.');
    })
    .catch(error => console.error('Error deleting product:', error));
